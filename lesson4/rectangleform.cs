﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lesson4
{
    public partial class rectangleform : Form
    {
        public rectangleform()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            MyRectangle rec = new MyRectangle(Convert.ToInt32(width.Text), Convert.ToInt32(height.Text));

            Console.WriteLine("The area is {0}", rec.GetArea());
            Console.WriteLine("The width is {0}",rec.GetPerimeter());
        }


    }
}
